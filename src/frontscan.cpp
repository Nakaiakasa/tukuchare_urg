#include <ros/ros.h>
#include "std_msgs/Int32.h"

#include <sensor_msgs/LaserScan.h>
#define range_near 0.6


void cb_scan(const sensor_msgs::LaserScan::ConstPtr &msg)
{
	int i = msg->ranges.size() / 2;
	//
	std_msgs::Int32 able2go;
	//
	if(msg->ranges[i] < msg->range_min || // エラー値の場合
		msg->ranges[i] > msg->range_max || // 測定範囲外の場合
		std::isnan(msg->ranges[i])) // 無限遠の場合
	{
		ROS_INFO("front-range: measurement error");
	}
	else
	{
		ROS_INFO("front-range: %0.3f",
			msg->ranges[msg->ranges.size() / 2]);
		if(msg->ranges[i]>range_near)able2go.data=1;//障害物なし
			else able2go.data=0;//障害物あり,止まれ
//		pub_go.publish(msg);
		ROS_INFO("able2go=%d\n",able2go.data);
	}
}


int main(int argc,char **argv){
	ros::init(argc,argv,"frontscan");
	ros::NodeHandle nh;
	//
	ros::Publisher pub_go = nh.advertise<std_msgs::Int32>("go", 1000);
	//
	ros::Subscriber sub_scan = nh.subscribe("/scan", 5,cb_scan);//this
	ros::Rate rate(1);
	ros::spin();
	


}
